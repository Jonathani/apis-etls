package com.gob.ec.sercop.apisetlsriesgos.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ArbolesFamiliaresResponseDto {

	String idArbolesFamiliares;
	
	String cedula;
	
	String cedulaConyuge;
	
	String cedulaMadre;
	
	String cedulaPadre;
	
	String estadoCivil;
	
	String fechaNacimiento;
	
	String identificacionUsuarioConsultado;
	
	String nivel;
	
	String nombreConyuge;
	
	String nombresMadre;
	
	String nombresPadre;
	
	String nombresUsuarioConsultado;
	
	String tipo;
	
}
