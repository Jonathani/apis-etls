package com.gob.ec.sercop.apisetlsriesgos.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gob.ec.sercop.apisetlsriesgos.model.DireccionesComercialSoce;

@Repository
public interface DireccionesComercialesSoceDao extends CrudRepository<DireccionesComercialSoce, Long>{
	
	@Transactional(readOnly = true)
	@Query(value = "SELECT direccionesComercialesSoce "
			+ "FROM DireccionesComercialSoce direccionesComercialesSoce "
			+ "JOIN direccionesComercialesSoce.variablesConsulta variablesConsulta "			
			+ "WHERE variablesConsulta.idVariablesConsulta = :idVariablesConsulta")
	List<DireccionesComercialSoce> findByIdVariablesConsulta(long idVariablesConsulta);

}
