package com.gob.ec.sercop.apisetlsriesgos.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gob.ec.sercop.apisetlsriesgos.dao.AutoridadesDao;
import com.gob.ec.sercop.apisetlsriesgos.dto.AutoridadesResponseDto;
import com.gob.ec.sercop.apisetlsriesgos.dto.IdVariableConsultaRequestDto;
import com.gob.ec.sercop.apisetlsriesgos.model.Autoridades;
import com.gob.ec.sercop.apisetlsriesgos.service.AutoridadesService;

@Service
public class AutoridadesServiceImpl implements AutoridadesService{
	
	@Autowired
	AutoridadesDao autoridadesDao;

	@Override
	public List<AutoridadesResponseDto> listaAutoridadesResponseDto(IdVariableConsultaRequestDto idVariableConsultaRequestDto) {
		
		List<AutoridadesResponseDto> listaAutoridadesResponseDto = new ArrayList<>();
		List<Autoridades> autoridades = autoridadesDao.findByIdVariablesConsulta(Long.parseLong(idVariableConsultaRequestDto.getIdVariableConsulta()));
		
		for (Autoridades autoridad :autoridades) {
			listaAutoridadesResponseDto.add(
					new AutoridadesResponseDto(
						String.valueOf(autoridad.getIdAutoridades()),
						autoridad.getIdSoliCompra(),
						autoridad.getCedula(),
						autoridad.getNombre(),
						autoridad.getFechaRegistro(),
						autoridad.getCargo()
					)
			);
		}		
		return listaAutoridadesResponseDto;
	}

}
