package com.gob.ec.sercop.apisetlsriesgos.restController;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.gob.ec.sercop.apisetlsriesgos.dto.EtlRiesgoFacticoRequestDto;
import com.gob.ec.sercop.apisetlsriesgos.dto.EtlRiesgoFacticoResponseDto;
import com.gob.ec.sercop.apisetlsriesgos.service.EtlsRiesgoFacticoService;
import com.gob.ec.sercop.utilitarioGeneral.properties.UtilitarioPropertiesEtls;

@RestController
@RequestMapping("api/etlRiesgoFacticoController")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class EtlsRiesgoFacticoController {
	
	@Autowired
	EtlsRiesgoFacticoService etlsRiesgoFacticoService;

	public static final String MENSAJE = "mensaje";
	
	@PostMapping(value = "ejecutarRiesgoFactico", produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> ejecutarRiesgoFactico(@RequestBody EtlRiesgoFacticoRequestDto etlRiesgoFacticoRequestDto) {
		
		Map<String, Object> responseMap = new HashMap<>();
		EtlRiesgoFacticoResponseDto etlRiesgoFacticoResponseDto; 
		Boolean estadoEtl = false;
		String mensajeRespuesta = "";
		try {
			String idVariablesConsulta = etlsRiesgoFacticoService.registrarEtlRiesgoFactico(etlRiesgoFacticoRequestDto);
			estadoEtl = etlsRiesgoFacticoService.ejecutarEtlRiesgoFactico(etlRiesgoFacticoRequestDto, idVariablesConsulta) ? true : false;
			mensajeRespuesta = estadoEtl ? UtilitarioPropertiesEtls.ETL_RESPUESTA_CORRECTA : UtilitarioPropertiesEtls.ETL_RESPUESTA_INCORRECTA;
			etlRiesgoFacticoResponseDto = new EtlRiesgoFacticoResponseDto(estadoEtl, mensajeRespuesta);
			
		}catch (Exception e) {
			responseMap.put(MENSAJE, "Error");
			responseMap.put("mensaje", e.toString());
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<EtlRiesgoFacticoResponseDto>(etlRiesgoFacticoResponseDto, HttpStatus.OK);
	}
}
