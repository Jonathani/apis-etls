package com.gob.ec.sercop.apisetlsriesgos.service;

import java.util.List;

import com.gob.ec.sercop.apisetlsriesgos.dto.AccionistasSuperciasDeResponseDto;
import com.gob.ec.sercop.apisetlsriesgos.dto.IdVariableConsultaRequestDto;

public interface AccionistasSuperciasDeService {
	
	List<AccionistasSuperciasDeResponseDto> listaAccionistasSuperciasDeResponseDto(IdVariableConsultaRequestDto idVariableConsultaRequestDto);

}
