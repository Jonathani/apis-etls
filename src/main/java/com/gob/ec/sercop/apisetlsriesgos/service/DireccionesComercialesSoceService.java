package com.gob.ec.sercop.apisetlsriesgos.service;

import java.util.List;

import com.gob.ec.sercop.apisetlsriesgos.dto.DireccionesComercialSoceResponseDto;
import com.gob.ec.sercop.apisetlsriesgos.dto.IdVariableConsultaRequestDto;

public interface DireccionesComercialesSoceService {
	
	List<DireccionesComercialSoceResponseDto> listaDireccionesComercialesSoceResponseDto(IdVariableConsultaRequestDto idVariableConsultaRequestDto);

}
