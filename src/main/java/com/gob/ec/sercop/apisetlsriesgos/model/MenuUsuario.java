package com.gob.ec.sercop.apisetlsriesgos.model;

import java.io.Serializable;
import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;


/**
 * The persistent class for the menu_usuario database table.
 * 
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="menu_usuario", schema = "arboles_familiares")
@NamedQuery(name="MenuUsuario.findAll", query="SELECT m FROM MenuUsuario m")
public class MenuUsuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "menu_usuario_id_seq", sequenceName = "public.menu_usuario_id_seq", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "menu_usuario_id_seq")
	@Column(name="id_menu_usuario")
	private long idMenuUsuario;

	@Column(name="created_at")
	private Timestamp createdAt;

	@Column(name="estado")
	private Boolean estado;

	@Column(name="update_at")
	private Timestamp updateAt;

	//bi-directional many-to-one association to Menu
	@ManyToOne
	@JoinColumn(name="id_menu")
	private Menu menu;

	//bi-directional many-to-one association to Usuario
	@ManyToOne
	@JoinColumn(name="id_usuario")
	private Usuario usuario;	

}