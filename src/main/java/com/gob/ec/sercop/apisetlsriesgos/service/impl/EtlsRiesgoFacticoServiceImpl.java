package com.gob.ec.sercop.apisetlsriesgos.service.impl;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.pentaho.di.core.KettleEnvironment;
import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.core.logging.LogLevel;
import org.pentaho.di.core.plugins.PluginFolder;
import org.pentaho.di.core.plugins.StepPluginType;
import org.pentaho.di.job.Job;
import org.pentaho.di.job.JobMeta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gob.ec.sercop.apisetlsriesgos.dao.UsuarioDao;
import com.gob.ec.sercop.apisetlsriesgos.dao.VariablesConsultaDao;
import com.gob.ec.sercop.apisetlsriesgos.dto.EtlRiesgoFacticoRequestDto;
import com.gob.ec.sercop.apisetlsriesgos.model.Usuario;
import com.gob.ec.sercop.apisetlsriesgos.model.VariablesConsulta;
import com.gob.ec.sercop.apisetlsriesgos.service.EtlsRiesgoFacticoService;
import com.gob.ec.sercop.utilitarioGeneral.properties.UtilitarioPropertiesEtls;

@Service
public class EtlsRiesgoFacticoServiceImpl implements EtlsRiesgoFacticoService{
	
	@Autowired
	VariablesConsultaDao variablesConsultaDao;
	
	@Autowired
	UsuarioDao usuarioDao;

	@Override
	public Boolean ejecutarEtlRiesgoFactico(EtlRiesgoFacticoRequestDto etlRiesgoFacticoRequestDto, String idVariablesConsulta) {
		Boolean statusEtl = false;
		try {
			StepPluginType.getInstance().getPluginFolders().add(new PluginFolder(UtilitarioPropertiesEtls.PLUGINS_PENTAHO, false, true));
			KettleEnvironment.init();
			JobMeta jobMeta = new JobMeta(UtilitarioPropertiesEtls.ELTS_JOB_RIESGO_FACTICO, null);			
			Job job = new Job(null, jobMeta);
			job.setVariable("fecha", etlRiesgoFacticoRequestDto.getFecha());
			job.setVariable("personas_extras", etlRiesgoFacticoRequestDto.getPersonasExtra());
			job.setVariable("codigos", etlRiesgoFacticoRequestDto.getCodigos());
			job.setVariable("idVariablesConsulta", idVariablesConsulta);
			job.setLogLevel(LogLevel.BASIC);
			job.start();
			job.waitUntilFinished();
			if (job.getErrors() == 0) {
				statusEtl = true;
			}
		} catch (KettleException e) {
			System.out.println(e.getMessage());
		}		
		return statusEtl;
	}

	@Override
	public String registrarEtlRiesgoFactico(EtlRiesgoFacticoRequestDto etlRiesgoFacticoRequestDto) {
		
		String pattern = "yyyy-MM-dd HH:mm:ss";
		LocalDateTime now = LocalDateTime.now();
		Timestamp fechaActual = Timestamp.valueOf(now);
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
		LocalDateTime localDateTime = LocalDateTime.from(formatter.parse(etlRiesgoFacticoRequestDto.getFecha().replace("'", "")));
		Timestamp fechaIngreso = Timestamp.valueOf(localDateTime);
		
		Usuario usuario = usuarioDao.findUsuarioById(Long.parseLong(etlRiesgoFacticoRequestDto.getIdUsuario()));
		
		VariablesConsulta variablesConsulta = new VariablesConsulta();
		variablesConsulta.setCodigo(etlRiesgoFacticoRequestDto.getCodigos().replace("'", ""));
		variablesConsulta.setFecha(fechaIngreso);
		variablesConsulta.setPersonasExtra(etlRiesgoFacticoRequestDto.getPersonasExtra());
		variablesConsulta.setCreatedAt(fechaActual);
		variablesConsulta.setEstado(true);
		variablesConsulta.setUsuario(usuario);
		variablesConsultaDao.save(variablesConsulta);
		
		return String.valueOf(variablesConsulta.getIdVariablesConsulta());
	}
	
	

}
