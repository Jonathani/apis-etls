package com.gob.ec.sercop.apisetlsriesgos.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gob.ec.sercop.apisetlsriesgos.model.AccionistasSuperciasDe;

@Repository
public interface AccionistasSuperciasDeDao extends CrudRepository<AccionistasSuperciasDe, Long>{

	@Transactional(readOnly = true)
	@Query(value = "SELECT accionistasSuperciasDe "
			+ "FROM AccionistasSuperciasDe accionistasSuperciasDe "
			+ "JOIN accionistasSuperciasDe.variablesConsulta variablesConsulta "			
			+ "WHERE variablesConsulta.idVariablesConsulta = :idVariablesConsulta")
	List<AccionistasSuperciasDe> findByIdVariablesConsulta(long idVariablesConsulta);
}
