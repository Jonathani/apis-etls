package com.gob.ec.sercop.apisetlsriesgos.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gob.ec.sercop.apisetlsriesgos.dao.DireccionesComercialesSriDao;
import com.gob.ec.sercop.apisetlsriesgos.dto.DireccionesComercialSriResponseDto;
import com.gob.ec.sercop.apisetlsriesgos.dto.IdVariableConsultaRequestDto;
import com.gob.ec.sercop.apisetlsriesgos.model.DireccionesComercialSri;
import com.gob.ec.sercop.apisetlsriesgos.service.DireccionesComercialesSriService;

@Service
public class DireccionesComercialSriServiceImpl implements DireccionesComercialesSriService{

	@Autowired
	DireccionesComercialesSriDao direccionesComercialesSriDao;
	
	@Override
	public List<DireccionesComercialSriResponseDto> listaDireccionesComercialesSriResponseDto(
			IdVariableConsultaRequestDto idVariableConsultaRequestDto) {
		
		List<DireccionesComercialSriResponseDto> listDireccionesComercialSriResponseDto = new ArrayList<>();
		List<DireccionesComercialSri> listaDireccionesComercialSri = direccionesComercialesSriDao.findByIdVariablesConsulta(Long.parseLong(idVariableConsultaRequestDto.getIdVariableConsulta()));
		
		for (DireccionesComercialSri direccionesComercialSri :listaDireccionesComercialSri) {
			listDireccionesComercialSriResponseDto.add(
					new DireccionesComercialSriResponseDto(
							String.valueOf(direccionesComercialSri.getIdDireccionesComercialSri()), 
							direccionesComercialSri.getNumeroRuc(), 
							direccionesComercialSri.getRazonSocial(), 
							direccionesComercialSri.getUbicacionGeografica(), 
							direccionesComercialSri.getCalle(), 
							direccionesComercialSri.getNumero(), 
							direccionesComercialSri.getInterseccion(), 
							direccionesComercialSri.getReferenciaUbicacion(), 
							direccionesComercialSri.getProvincia(), 
							direccionesComercialSri.getDescripcion(), 
							direccionesComercialSri.getEstadoEstablecimiento())
			);
		}		
		return listDireccionesComercialSriResponseDto;
	}	

}
