package com.gob.ec.sercop.apisetlsriesgos.model;

import java.io.Serializable;
import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;


/**
 * The persistent class for the menu database table.
 * 
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="menu", schema = "arboles_familiares")
@NamedQuery(name="Menu.findAll", query="SELECT m FROM Menu m")
public class Menu implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "menu_id_seq", sequenceName = "public.menu_id_seq", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "menu_id_seq")
	@Column(name="id_menu")
	private long idMenu;

	@Column(name="desplegar_menu")
	private Boolean desplegarMenu;

	@Column(name="estado")
	private Boolean estado;

	@Column(name="nombre_icono_false")
	private String nombreIconoFalse;

	@Column(name="nombre_icono_principal")
	private String nombreIconoPrincipal;

	@Column(name="nombre_icono_true")
	private String nombreIconoTrue;

	@Column(name="nombre_label")
	private String nombreLabel;

	@Column(name="nombre_metodo_principal")
	private String nombreMetodoPrincipal;

	@Column(name="nombre_metodo_secundario")
	private String nombreMetodoSecundario;

	//bi-directional many-to-one association to Menu
	@ManyToOne
	@JoinColumn(name="id_menu_padre")
	private Menu menu;

	//bi-directional many-to-one association to Menu
	@OneToMany(mappedBy="menu")
	private List<Menu> menus;

	//bi-directional many-to-one association to MenuUsuario
	@OneToMany(mappedBy="menu")
	private List<MenuUsuario> menuUsuarios;	

}