package com.gob.ec.sercop.apisetlsriesgos.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class VariablesConsultaResponseDto {

	String idVariableConsulta;
	
	String fechaDesde;
	
	String personasExtra;
	
	String codigo;
	
	String fechaCreacion;
}
