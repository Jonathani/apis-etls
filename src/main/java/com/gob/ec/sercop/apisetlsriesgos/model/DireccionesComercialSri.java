package com.gob.ec.sercop.apisetlsriesgos.model;

import java.io.Serializable;
import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * The persistent class for the direcciones_comercial_sri database table.
 * 
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="direcciones_comercial_sri", schema = "arboles_familiares")
@NamedQuery(name="DireccionesComercialSri.findAll", query="SELECT d FROM DireccionesComercialSri d")
public class DireccionesComercialSri implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "direcciones_comercial_sri_id_seq", sequenceName = "public.direcciones_comercial_sri_id_seq", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "direcciones_comercial_sri_id_seq")
	@Column(name="id_direcciones_comercial_sri")
	private long idDireccionesComercialSri;

	@Column(name="calle")
	private String calle;

	@Column(name="descripcion")
	private String descripcion;

	@Column(name="estadoestablecimiento")
	private String estadoEstablecimiento;

	@Column(name="interseccion")
	private String interseccion;

	@Column(name="numero")
	private String numero;
	
	@Column(name="numeroruc")
	private String numeroRuc;

	@Column(name="provincia")
	private String provincia;

	@Column(name="razonsocial")
	private String razonSocial;

	@Column(name="referenciaubicacion")
	private String referenciaUbicacion;

	@Column(name="ubicaciongeografica")
	private String ubicacionGeografica;

	//bi-directional many-to-one association to VariablesConsulta
	@ManyToOne
	@JoinColumn(name="id_variables_consulta")
	private VariablesConsulta variablesConsulta;	

}