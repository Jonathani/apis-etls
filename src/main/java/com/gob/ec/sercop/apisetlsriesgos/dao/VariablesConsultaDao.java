package com.gob.ec.sercop.apisetlsriesgos.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gob.ec.sercop.apisetlsriesgos.model.VariablesConsulta;


@Repository
public interface VariablesConsultaDao extends CrudRepository<VariablesConsulta, Long>{

	@Transactional(readOnly = true)
	@Query(value = "SELECT variablesConsulta "
			+ "FROM VariablesConsulta variablesConsulta "
			+ "order by variablesConsulta.createdAt desc")
	List<VariablesConsulta> findByAll();
	
	@Transactional(readOnly = true)
	@Query(value = "SELECT variablesConsulta "
			+ "FROM VariablesConsulta variablesConsulta "
			+ "JOIN variablesConsulta.usuario usuario "
			+ "where usuario.idUsuario = :idUsuario "
			+ "order by variablesConsulta.createdAt desc")
	List<VariablesConsulta> findByUser(long idUsuario);
	
}
