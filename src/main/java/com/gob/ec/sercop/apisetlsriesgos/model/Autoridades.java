package com.gob.ec.sercop.apisetlsriesgos.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * The persistent class for the autoridades database table.
 * 
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="autoridades", schema = "arboles_familiares")
@NamedQuery(name="Autoridades.findAll", query="SELECT a FROM Autoridades a")
public class Autoridades implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "autoridades_id_seq", sequenceName = "public.autoridades_id_seq", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "autoridades_id_seq")
	@Column(name="id_autoridades")
	private long idAutoridades;

	@Column(name="cargo")
	private String cargo;

	@Column(name="cedula")
	private String cedula;

	@Column(name="fecha_registro")
	private String fechaRegistro;

	@Column(name="id_soli_compra")
	private String idSoliCompra;
	
	@Column(name="nombre")
	private String nombre;
	
	//bi-directional many-to-one association to VariablesConsulta
	@ManyToOne
	@JoinColumn(name="id_variables_consulta")
	private VariablesConsulta variablesConsulta;
	
}