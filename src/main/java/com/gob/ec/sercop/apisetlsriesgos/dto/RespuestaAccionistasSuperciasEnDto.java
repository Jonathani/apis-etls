package com.gob.ec.sercop.apisetlsriesgos.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RespuestaAccionistasSuperciasEnDto {
	
	Boolean estado;
	
	List<AccionistasSuperciasEnResponseDto> listaRespuesta;

}
