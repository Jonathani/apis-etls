package com.gob.ec.sercop.apisetlsriesgos.service;

import java.util.List;

import com.gob.ec.sercop.apisetlsriesgos.dto.ComisionResponseDto;
import com.gob.ec.sercop.apisetlsriesgos.dto.IdVariableConsultaRequestDto;

public interface ComisionService {
	
	List<ComisionResponseDto> listaComisionResponseDto(IdVariableConsultaRequestDto idVariableConsultaRequestDto);

}
