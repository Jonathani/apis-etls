package com.gob.ec.sercop.apisetlsriesgos.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gob.ec.sercop.apisetlsriesgos.model.Usuario;

@Repository
public interface UsuarioDao extends CrudRepository<Usuario, Long>{
	
	@Transactional(readOnly = true)
	@Query(value = "SELECT usuario "
			+ "FROM Usuario usuario "		
			+ "WHERE usuario.idUsuario = :idUsuario")
	Usuario findUsuarioById(long idUsuario);

}
