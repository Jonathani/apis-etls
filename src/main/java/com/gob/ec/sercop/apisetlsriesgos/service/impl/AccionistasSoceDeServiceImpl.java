package com.gob.ec.sercop.apisetlsriesgos.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gob.ec.sercop.apisetlsriesgos.dao.AccionistasSoceDeDao;
import com.gob.ec.sercop.apisetlsriesgos.dto.AccionistasSoceDeResponseDto;
import com.gob.ec.sercop.apisetlsriesgos.dto.IdVariableConsultaRequestDto;
import com.gob.ec.sercop.apisetlsriesgos.model.AccionistasSoceDe;
import com.gob.ec.sercop.apisetlsriesgos.service.AccionistasSoceDeService;

@Service
public class AccionistasSoceDeServiceImpl implements AccionistasSoceDeService{
	
	@Autowired
	AccionistasSoceDeDao accionistasSoceDeDao; 

	@Override
	public List<AccionistasSoceDeResponseDto> listaAccionistasSoceDeResponseDto(IdVariableConsultaRequestDto idVariableConsultaRequestDto) {
		
		List<AccionistasSoceDeResponseDto> listaAccionistasSoceDeResponseDto = new ArrayList<>(); 
		List<AccionistasSoceDe> listaAccionistasSoceDe = accionistasSoceDeDao.findByIdVariablesConsulta(Long.parseLong(idVariableConsultaRequestDto.getIdVariableConsulta()));
		for (AccionistasSoceDe accionistasSoceDe: listaAccionistasSoceDe) {
			listaAccionistasSoceDeResponseDto.add(
				new AccionistasSoceDeResponseDto(
					String.valueOf(accionistasSoceDe.getIdAccionistasSoceDe()),
					accionistasSoceDe.getRuc(),
					accionistasSoceDe.getRazonSocial(),
					accionistasSoceDe.getAccionistaCedRuc(),
					accionistasSoceDe.getNombreComercial(),
					accionistasSoceDe.getAccionistaNombresApellidos()
				)
			);
		}
		return listaAccionistasSoceDeResponseDto;
	}	

}
