package com.gob.ec.sercop.apisetlsriesgos.service;

import java.util.List;

import com.gob.ec.sercop.apisetlsriesgos.dto.ConsorcioResponseDto;
import com.gob.ec.sercop.apisetlsriesgos.dto.IdVariableConsultaRequestDto;

public interface ConsorcioService {
	
	List<ConsorcioResponseDto> listaConsorcioResponseDto(IdVariableConsultaRequestDto idVariableConsultaRequestDto);

}
