package com.gob.ec.sercop.apisetlsriesgos.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gob.ec.sercop.apisetlsriesgos.dao.VariablesConsultaDao;
import com.gob.ec.sercop.apisetlsriesgos.dto.VariablesConsultaRequestDto;
import com.gob.ec.sercop.apisetlsriesgos.dto.VariablesConsultaResponseDto;
import com.gob.ec.sercop.apisetlsriesgos.model.VariablesConsulta;
import com.gob.ec.sercop.apisetlsriesgos.service.VariablesConsultaService;
import com.gob.ec.sercop.utilitarioGeneral.properties.UtilitarioPropertiesEtls;

@Service
public class VariablesConsultaServiceImpl implements VariablesConsultaService {

	@Autowired
	VariablesConsultaDao variablesConsultaDao;

	@Override
	public List<VariablesConsultaResponseDto> listarVariablesConsulta(VariablesConsultaRequestDto variablesConsultaRequestDto) {

		List<VariablesConsultaResponseDto> listaVariablesConsulta = new ArrayList<>();
		List<VariablesConsulta> variablesDeConsulta = null;		
		
		if (variablesConsultaRequestDto.getIdPerfil().equals(UtilitarioPropertiesEtls.PERFIL_ADMINISTRADOR)) {
			variablesDeConsulta = variablesConsultaDao.findByAll();
		} else if (variablesConsultaRequestDto.getIdPerfil().equals(UtilitarioPropertiesEtls.PERFIL_ANALISTA)) {
			variablesDeConsulta = variablesConsultaDao.findByUser(Integer.parseInt(variablesConsultaRequestDto.getIdUsuario()));
		}
		for (VariablesConsulta variablesConsulta : variablesDeConsulta) {
			listaVariablesConsulta.add(
					new VariablesConsultaResponseDto(
							String.valueOf(variablesConsulta.getIdVariablesConsulta()),
							String.valueOf(variablesConsulta.getFecha()),
							variablesConsulta.getPersonasExtra(),
							variablesConsulta.getCodigo(),
							String.valueOf(variablesConsulta.getCreatedAt()))
					);
		}

		return listaVariablesConsulta;
	}

}
