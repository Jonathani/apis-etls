package com.gob.ec.sercop.apisetlsriesgos.service;

import java.util.List;

import com.gob.ec.sercop.apisetlsriesgos.dto.IdVariableConsultaRequestDto;
import com.gob.ec.sercop.apisetlsriesgos.dto.OfertasResponseDto;

public interface OfertasService {
	
	List<OfertasResponseDto> listaOfertasResponseDto(IdVariableConsultaRequestDto idVariableConsultaRequestDto);

}
