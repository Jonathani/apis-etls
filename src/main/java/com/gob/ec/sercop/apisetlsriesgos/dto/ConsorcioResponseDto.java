package com.gob.ec.sercop.apisetlsriesgos.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ConsorcioResponseDto {
	
	String consorcioId;
	
	String personaIdConsorcio;
	
	String razonSocialConsorcio;
	
	String idSoliCompra;
	
	String fechaRegistro;
	
	String personaIdParticipante;
	
	String cedulaParticipante;
	
	String razonSocialParticipante;
	
	String fechaRegistroParticipante;
	
	String idConsorcio;

}
