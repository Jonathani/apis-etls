package com.gob.ec.sercop.apisetlsriesgos.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gob.ec.sercop.apisetlsriesgos.dao.OfertasDao;
import com.gob.ec.sercop.apisetlsriesgos.dto.IdVariableConsultaRequestDto;
import com.gob.ec.sercop.apisetlsriesgos.dto.OfertasResponseDto;
import com.gob.ec.sercop.apisetlsriesgos.model.Oferta;
import com.gob.ec.sercop.apisetlsriesgos.service.OfertasService;

@Service
public class OfertasServiceImpl implements OfertasService{
	
	@Autowired
	OfertasDao ofertasDao; 

	@Override
	public List<OfertasResponseDto> listaOfertasResponseDto(IdVariableConsultaRequestDto idVariableConsultaRequestDto) {
		
		List<OfertasResponseDto> listaOfertasResponseDto = new ArrayList<>();
		List<Oferta> ofertas = ofertasDao.findByIdVariablesConsulta(Long.parseLong(idVariableConsultaRequestDto.getIdVariableConsulta()));
		
		for (Oferta oferta: ofertas) {
			listaOfertasResponseDto.add(
				new OfertasResponseDto(
						String.valueOf(oferta.getIdOfertas()),
						oferta.getIdSoliCompra(), 
						oferta.getCodigoProcedimiento(), 
						oferta.getObjetoProceso(), 
						oferta.getNombreContratante(), 
						oferta.getRucContratante(), 
						oferta.getTipoProceso(), 
						oferta.getFechaPublicacion(), 
						oferta.getPresupuesto(), 
						oferta.getEstado(), 
						oferta.getFechPropuesta(), 
						oferta.getValorPropuesta(), 
						oferta.getIdParticipante(), 
						oferta.getRazonSocialParticipante())
			);
		}
		
		return listaOfertasResponseDto;
	}
	
	

}
