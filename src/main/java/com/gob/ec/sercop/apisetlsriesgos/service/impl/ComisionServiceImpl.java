package com.gob.ec.sercop.apisetlsriesgos.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gob.ec.sercop.apisetlsriesgos.dao.ComisionDao;
import com.gob.ec.sercop.apisetlsriesgos.dto.ComisionResponseDto;
import com.gob.ec.sercop.apisetlsriesgos.dto.IdVariableConsultaRequestDto;
import com.gob.ec.sercop.apisetlsriesgos.model.Comision;
import com.gob.ec.sercop.apisetlsriesgos.service.ComisionService;

@Service
public class ComisionServiceImpl implements ComisionService{

	@Autowired
	ComisionDao comisionDao;
	
	@Override
	public List<ComisionResponseDto> listaComisionResponseDto(IdVariableConsultaRequestDto idVariableConsultaRequestDto) {
		
		List<Comision> comisiones = comisionDao.findByIdVariablesConsulta(Long.parseLong(idVariableConsultaRequestDto.getIdVariableConsulta()));
		List<ComisionResponseDto> listaComisionResponseDto = new ArrayList<>();
		
		for (Comision comision: comisiones) {
			listaComisionResponseDto.add(
				new ComisionResponseDto(
						String.valueOf(comision.getIdComision()), 
						comision.getIdSoliCompra(), 
						comision.getCedula(), 
						comision.getNombre(), 
						comision.getCargo(), 
						comision.getFuncion())	
			);
		}
		
		return listaComisionResponseDto;
	}

}
