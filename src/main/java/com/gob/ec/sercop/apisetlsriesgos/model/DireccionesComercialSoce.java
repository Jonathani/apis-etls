package com.gob.ec.sercop.apisetlsriesgos.model;

import java.io.Serializable;
import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * The persistent class for the direcciones_comercial_soce database table.
 * 
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="direcciones_comercial_soce", schema = "arboles_familiares")
@NamedQuery(name="DireccionesComercialSoce.findAll", query="SELECT d FROM DireccionesComercialSoce d")
public class DireccionesComercialSoce implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "direcciones_comercial_soce_id_seq", sequenceName = "public.direcciones_comercial_soce_id_seq", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "direcciones_comercial_soce_id_seq")
	@Column(name="id_direcciones_comercial_soce")
	private long idDireccionesComercialSoce;

	@Column(name="calle")
	private String calle;

	@Column(name="cedula")
	private String cedula;

	@Column(name="ciudad")
	private String ciudad;

	@Column(name="cod_pais")
	private String codPais;

	@Column(name="departamento")
	private String departamento;

	@Column(name="edificio")
	private String edificio;

	@Column(name="interseccion")
	private String interseccion;

	@Column(name="numero")
	private String numero;

	@Column(name="parroquia")
	private String parroquia;

	@Column(name="provincia")
	private String provincia;

	@Column(name="razon_social")
	private String razonSocial;

	//bi-directional many-to-one association to VariablesConsulta
	@ManyToOne
	@JoinColumn(name="id_variables_consulta")
	private VariablesConsulta variablesConsulta;	

}