package com.gob.ec.sercop.apisetlsriesgos.model;

import java.io.Serializable;
import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;


/**
 * The persistent class for the perfil database table.
 * 
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="perfil", schema = "arboles_familiares")
@NamedQuery(name="Perfil.findAll", query="SELECT p FROM Perfil p")
public class Perfil implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "perfil_id_seq", sequenceName = "public.perfil_id_seq", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "perfil_id_seq")
	@Column(name="id_perfil")
	private long idPerfil;

	@Column(name="descripcion")
	private String descripcion;

	@Column(name="estado")
	private Boolean estado;

	//bi-directional many-to-one association to PerfilUsuario
	@OneToMany(mappedBy="perfil")
	private List<PerfilUsuario> perfilUsuarios;	

}