package com.gob.ec.sercop.apisetlsriesgos.restController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.gob.ec.sercop.apisetlsriesgos.dto.ArbolesFamiliaresResponseDto;
import com.gob.ec.sercop.apisetlsriesgos.dto.IdVariableConsultaRequestDto;
import com.gob.ec.sercop.apisetlsriesgos.dto.RespuestaArbolesFamiliaresDto;
import com.gob.ec.sercop.apisetlsriesgos.service.ArbolesFamiliaresService;

@RestController
@RequestMapping("api/arbolesFamiliaresController")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class ArbolesFamiliaresController {
	
	@Autowired
	ArbolesFamiliaresService arbolesFamiliaresService;
	
	public static final String MENSAJE = "mensaje";
	
	@PostMapping(value = "mostrarArbolesFamiliares", produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> mostrarArbolesFamiliares(@RequestBody IdVariableConsultaRequestDto idVariableConsultaRequestDto) {
		
		Map<String, Object> responseMap = new HashMap<>();
		RespuestaArbolesFamiliaresDto respuestaArbolesFamiliaresDto;
		List<ArbolesFamiliaresResponseDto> arbolesFamiliaresResponseDto = new ArrayList<>();
		try {
			arbolesFamiliaresResponseDto= arbolesFamiliaresService.listaArbolesFamiliaresResponseDto(idVariableConsultaRequestDto);
			respuestaArbolesFamiliaresDto = new RespuestaArbolesFamiliaresDto(true, arbolesFamiliaresResponseDto);
			
		}catch (Exception e) {
			responseMap.put(MENSAJE, "Error");
			responseMap.put("mensaje", e.toString());
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<RespuestaArbolesFamiliaresDto>(respuestaArbolesFamiliaresDto, HttpStatus.OK);		
		
	}

}
