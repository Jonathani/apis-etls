package com.gob.ec.sercop.apisetlsriesgos;

import java.util.Collections;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApisEtlsRiesgosApplication {

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(ApisEtlsRiesgosApplication.class);
        app.setDefaultProperties(Collections.singletonMap("server.port", "8084"));
        app.run(args);
	}

}
