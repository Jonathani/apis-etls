package com.gob.ec.sercop.apisetlsriesgos.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * The persistent class for the arboles_familiares database table.
 * 
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="arboles_familiares", schema = "arboles_familiares")
@NamedQuery(name="ArbolesFamiliares.findAll", query="SELECT a FROM ArbolesFamiliares a")
public class ArbolesFamiliares implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "arboles_familiares_id_seq", sequenceName = "public.arboles_familiares_id_seq", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "arboles_familiares_id_seq")
	@Column(name="id_arboles_familiares")
	private long idArbolesFamiliares;

	@Column(name="cedula")
	private String cedula;
	
	@Column(name="cedulaconyuge")
	private String cedulaConyuge;

	@Column(name="cedulamadre")
	private String cedulaMadre;

	@Column(name="cedulapadre")
	private String cedulaPadre;

	@Column(name="estadocivil")
	private String estadoCivil;

	@Column(name="fechanacimiento")
	private String fechaNacimiento;

	@Column(name="identificacionusuarioconsultado")
	private String identificacionUsuarioConsultado;

	@Column(name="nivel")
	private String nivel;

	@Column(name="nombreconyuge")
	private String nombreConyuge;

	@Column(name="nombresmadre")
	private String nombresMadre;

	@Column(name="nombrespadre")
	private String nombresPadre;

	@Column(name="nombresusuario")
	private String nombresUsuario;

	@Column(name="nombresusuarioconsultado")
	private String nombresUsuarioConsultado;

	@Column(name="tipo")
	private String tipo;

	//bi-directional many-to-one association to VariablesConsulta
	@ManyToOne
	@JoinColumn(name="id_variables_consulta")
	private VariablesConsulta variablesConsulta;

}