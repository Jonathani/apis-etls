package com.gob.ec.sercop.apisetlsriesgos.model;

import java.io.Serializable;
import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * The persistent class for the ofertas database table.
 * 
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="ofertas", schema = "arboles_familiares")
@NamedQuery(name="Oferta.findAll", query="SELECT o FROM Oferta o")
public class Oferta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "ofertas_id_seq", sequenceName = "public.ofertas_id_seq", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "ofertas_id_seq")
	@Column(name="id_ofertas")
	private long idOfertas;

	@Column(name="codigoprocedimiento")
	private String codigoProcedimiento;

	@Column(name="estado")
	private String estado;

	@Column(name="fech_propuesta")
	private String fechPropuesta;

	@Column(name="fecha_publicacion")
	private String fechaPublicacion;

	@Column(name="id_soli_compra")
	private String idSoliCompra;

	@Column(name="idparticipante")
	private String idParticipante;

	@Column(name="nombrecontratante")
	private String nombreContratante;

	@Column(name="objetoproceso")
	private String objetoProceso;

	@Column(name="presupuesto")
	private String presupuesto;

	@Column(name="razonsocialparticipante")
	private String razonSocialParticipante;

	@Column(name="ruccontratante")
	private String rucContratante;

	@Column(name="rucparticipante")
	private String rucParticipante;

	@Column(name="tipo_proceso")
	private String tipoProceso;

	@Column(name="valo_propuesta")
	private String valorPropuesta;

	//bi-directional many-to-one association to VariablesConsulta
	@ManyToOne
	@JoinColumn(name="id_variables_consulta")
	private VariablesConsulta variablesConsulta;

}