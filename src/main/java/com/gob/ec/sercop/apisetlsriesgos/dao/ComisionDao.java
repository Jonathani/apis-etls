package com.gob.ec.sercop.apisetlsriesgos.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gob.ec.sercop.apisetlsriesgos.model.Comision;

@Repository
public interface ComisionDao extends CrudRepository<Comision, Long>{
	
	@Transactional(readOnly = true)
	@Query(value = "SELECT comision "
			+ "FROM Comision comision "
			+ "JOIN comision.variablesConsulta variablesConsulta "			
			+ "WHERE variablesConsulta.idVariablesConsulta = :idVariablesConsulta")
	List<Comision> findByIdVariablesConsulta(long idVariablesConsulta);
	
}
