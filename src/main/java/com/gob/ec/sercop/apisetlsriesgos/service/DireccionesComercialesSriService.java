package com.gob.ec.sercop.apisetlsriesgos.service;

import java.util.List;

import com.gob.ec.sercop.apisetlsriesgos.dto.DireccionesComercialSriResponseDto;
import com.gob.ec.sercop.apisetlsriesgos.dto.IdVariableConsultaRequestDto;

public interface DireccionesComercialesSriService {
	
	List<DireccionesComercialSriResponseDto> listaDireccionesComercialesSriResponseDto(IdVariableConsultaRequestDto idVariableConsultaRequestDto);

}
