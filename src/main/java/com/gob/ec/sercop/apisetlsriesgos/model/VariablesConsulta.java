package com.gob.ec.sercop.apisetlsriesgos.model;

import java.io.Serializable;
import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the variables_consulta database table.
 * 
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="variables_consulta", schema = "arboles_familiares")
@NamedQuery(name="VariablesConsulta.findAll", query="SELECT v FROM VariablesConsulta v")
public class VariablesConsulta implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "variables_consulta_id_seq", sequenceName = "public.variables_consulta_id_seq", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "variables_consulta_id_seq")
	@Column(name="id_variables_consulta")
	private long idVariablesConsulta;

	@Column(name="codigo")
	private String codigo;

	@Column(name="created_at")
	private Timestamp createdAt;

	@Column(name="estado")
	private Boolean estado;

	@Column(name="fecha")
	private Timestamp fecha;

	@Column(name="personas_extra")
	private String personasExtra;

	@Column(name="updated_at")
	private Timestamp updatedAt;
	
	@ManyToOne
	@JoinColumn(name="id_usuario")
	private Usuario usuario;

	//bi-directional many-to-one association to AccionistasSoceDe
	@OneToMany(mappedBy="variablesConsulta")
	private List<AccionistasSoceDe> accionistasSoceDe;

	//bi-directional many-to-one association to AccionistasSuperciasDe
	@OneToMany(mappedBy="variablesConsulta")
	private List<AccionistasSuperciasDe> accionistasSuperciasDe;

	//bi-directional many-to-one association to AccionistasSuperciasEn
	@OneToMany(mappedBy="variablesConsulta")
	private List<AccionistasSuperciasEn> accionistasSuperciasEn;

	//bi-directional many-to-one association to ArbolesFamiliare
	@OneToMany(mappedBy="variablesConsulta")
	private List<ArbolesFamiliares> arbolesFamiliares;
	
	@OneToMany(mappedBy="variablesConsulta")
	private List<Autoridades> autoridades;

	//bi-directional many-to-one association to Comision
	@OneToMany(mappedBy="variablesConsulta")
	private List<Comision> comision;
	
	@OneToMany(mappedBy="variablesConsulta")
	private List<Consorcio> consorcio;

	//bi-directional many-to-one association to DireccionesComercialSoce
	@OneToMany(mappedBy="variablesConsulta")
	private List<DireccionesComercialSoce> direccionesComercialSoce;

	//bi-directional many-to-one association to DireccionesComercialSri
	@OneToMany(mappedBy="variablesConsulta")
	private List<DireccionesComercialSri> direccionesComercialSri;

	//bi-directional many-to-one association to Oferta
	@OneToMany(mappedBy="variablesConsulta")
	private List<Oferta> ofertas;	

}