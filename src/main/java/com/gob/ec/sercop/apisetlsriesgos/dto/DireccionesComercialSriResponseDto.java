package com.gob.ec.sercop.apisetlsriesgos.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DireccionesComercialSriResponseDto {

	String idDireccionesComercial;
	
	String numeroRuc;
	
	String razonSocial;
	
	String ubicacionGeografica;
	
	String calle;
	
	String numero;
	
	String interseccion;
	
	String referenciaUbicacion;
	
	String provincia;
	
	String canton;
	
	String estadoEstablecimiento;
	
}
