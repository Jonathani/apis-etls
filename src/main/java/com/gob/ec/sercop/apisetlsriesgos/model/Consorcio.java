package com.gob.ec.sercop.apisetlsriesgos.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * The persistent class for the consorcio database table.
 * 
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="consorcio", schema = "arboles_familiares")
@NamedQuery(name="Consorcio.findAll", query="SELECT c FROM Consorcio c")
public class Consorcio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "consorcio_id_seq", sequenceName = "public.consorcio_id_seq", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "consorcio_id_seq")
	@Column(name="consorcio_id")
	private long consorcioId;

	@Column(name="cedula_participante")
	private String cedulaParticipante;

	@Column(name="fecha_registro")
	private String fechaRegistro;

	@Column(name="fecha_registro_participante")
	private String fechaRegistroParticipante;

	@Column(name="id_consorcio")
	private String idConsorcio;

	@Column(name="id_soli_compra")
	private String idSoliCompra;

	@Column(name="persona_id_consorcio")
	private String personaIdConsorcio;

	@Column(name="persona_id_participante")
	private String personaIdParticipante;

	@Column(name="razon_social_consorcio")
	private String razonSocialConsorcio;

	@Column(name="razon_social_participante")
	private String razonSocialParticipante;
	
	//bi-directional many-to-one association to VariablesConsulta
	@ManyToOne
	@JoinColumn(name="id_variables_consulta")
	private VariablesConsulta variablesConsulta;	

}