package com.gob.ec.sercop.apisetlsriesgos.restController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.gob.ec.sercop.apisetlsriesgos.dto.ConsorcioResponseDto;
import com.gob.ec.sercop.apisetlsriesgos.dto.IdVariableConsultaRequestDto;
import com.gob.ec.sercop.apisetlsriesgos.dto.RespuestaConsorcioDto;
import com.gob.ec.sercop.apisetlsriesgos.service.ConsorcioService;

@RestController
@RequestMapping("api/consorcioController")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class ConsorcioController {

	@Autowired
	ConsorcioService consorcioService;
	
	public static final String MENSAJE = "mensaje";
	
	@PostMapping(value = "mostrarConsorcio", produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> mostrarConsorcio(@RequestBody IdVariableConsultaRequestDto idVariableConsultaRequestDto) {
	
	Map<String, Object> responseMap = new HashMap<>();
	RespuestaConsorcioDto respuestaConsorcioDto;

		List<ConsorcioResponseDto> consorcioResponseDto = new ArrayList<>();
		try {
			consorcioResponseDto= consorcioService.listaConsorcioResponseDto(idVariableConsultaRequestDto);
			respuestaConsorcioDto = new RespuestaConsorcioDto(true, consorcioResponseDto);
			
		}catch (Exception e) {
			responseMap.put(MENSAJE, "Error");
			responseMap.put("mensaje", e.toString());
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<RespuestaConsorcioDto>(respuestaConsorcioDto, HttpStatus.OK);
	}
	
}
