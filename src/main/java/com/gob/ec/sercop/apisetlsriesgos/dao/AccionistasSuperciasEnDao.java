package com.gob.ec.sercop.apisetlsriesgos.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gob.ec.sercop.apisetlsriesgos.model.AccionistasSuperciasEn;

@Repository
public interface AccionistasSuperciasEnDao extends CrudRepository<AccionistasSuperciasEn, Long>{

	@Transactional(readOnly = true)
	@Query(value = "SELECT accionistasSuperciasEn "
			+ "FROM AccionistasSuperciasEn accionistasSuperciasEn "
			+ "JOIN accionistasSuperciasEn.variablesConsulta variablesConsulta "			
			+ "WHERE variablesConsulta.idVariablesConsulta = :idVariablesConsulta")
	List<AccionistasSuperciasEn> findByIdVariablesConsulta(long idVariablesConsulta);
}
