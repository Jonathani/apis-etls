package com.gob.ec.sercop.apisetlsriesgos.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ComisionResponseDto {
	
	String idComision;
	
	String idSoliCompra;
	
	String cedula;
	
	String nombre;
	
	String cargo;
	
	String funcion;

}
