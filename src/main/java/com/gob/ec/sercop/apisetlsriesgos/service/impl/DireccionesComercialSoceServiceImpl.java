package com.gob.ec.sercop.apisetlsriesgos.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gob.ec.sercop.apisetlsriesgos.dao.DireccionesComercialesSoceDao;
import com.gob.ec.sercop.apisetlsriesgos.dto.DireccionesComercialSoceResponseDto;
import com.gob.ec.sercop.apisetlsriesgos.dto.IdVariableConsultaRequestDto;
import com.gob.ec.sercop.apisetlsriesgos.model.DireccionesComercialSoce;
import com.gob.ec.sercop.apisetlsriesgos.service.DireccionesComercialesSoceService;

@Service
public class DireccionesComercialSoceServiceImpl implements DireccionesComercialesSoceService{

	@Autowired
	DireccionesComercialesSoceDao direccionesComercialesSoceDao;
	
	@Override
	public List<DireccionesComercialSoceResponseDto> listaDireccionesComercialesSoceResponseDto(
			IdVariableConsultaRequestDto idVariableConsultaRequestDto) {
		
		List<DireccionesComercialSoceResponseDto> listDireccionesComercialSoceResponseDto = new ArrayList<>();
		List<DireccionesComercialSoce> listaDireccionesComercialSoce = direccionesComercialesSoceDao.findByIdVariablesConsulta(Long.parseLong(idVariableConsultaRequestDto.getIdVariableConsulta()));
		
		for (DireccionesComercialSoce direccionesComercialSoce: listaDireccionesComercialSoce) {
			listDireccionesComercialSoceResponseDto.add(
				new DireccionesComercialSoceResponseDto(
						String.valueOf(direccionesComercialSoce.getIdDireccionesComercialSoce()), 
						direccionesComercialSoce.getCedula(), 
						direccionesComercialSoce.getRazonSocial(), 
						direccionesComercialSoce.getCodPais(), 
						direccionesComercialSoce.getProvincia(), 
						direccionesComercialSoce.getCiudad(), 
						direccionesComercialSoce.getParroquia(), 
						direccionesComercialSoce.getCalle(), 
						direccionesComercialSoce.getInterseccion(), 
						direccionesComercialSoce.getNumero(), 
						direccionesComercialSoce.getEdificio(), 
						direccionesComercialSoce.getDepartamento())	
			);			
		}
		
		return listDireccionesComercialSoceResponseDto;
	}

}
