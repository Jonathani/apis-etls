package com.gob.ec.sercop.apisetlsriesgos.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gob.ec.sercop.apisetlsriesgos.model.DireccionesComercialSri;

@Repository
public interface DireccionesComercialesSriDao extends CrudRepository<DireccionesComercialSri, Long>{

	@Transactional(readOnly = true)
	@Query(value = "SELECT direccionesComercialSri "
			+ "FROM DireccionesComercialSri direccionesComercialSri "
			+ "JOIN direccionesComercialSri.variablesConsulta variablesConsulta "			
			+ "WHERE variablesConsulta.idVariablesConsulta = :idVariablesConsulta")
	List<DireccionesComercialSri> findByIdVariablesConsulta(long idVariablesConsulta);
}
