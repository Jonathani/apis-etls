package com.gob.ec.sercop.apisetlsriesgos.model;

import java.io.Serializable;
import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the usuario database table.
 * 
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="usuario", schema = "arboles_familiares")
@NamedQuery(name="Usuario.findAll", query="SELECT u FROM Usuario u")
public class Usuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "usuario_id_seq", sequenceName = "public.usuario_id_seq", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "usuario_id_seq")
	@Column(name="id_usuario")
	private long idUsuario;

	@Column(name="contrasenia")
	private String contrasenia;

	@Column(name="correo")
	private String correo;

	@Column(name="estado")
	private Boolean estado;

	@Column(name="estado_contrasenia")
	private Boolean estadoContrasenia;

	@Column(name="fecha_actualizacion")
	private Timestamp fechaActualizacion;

	@Column(name="fecha_creacion")
	private Timestamp fechaCreacion;

	@Column(name="identificacion")
	private String identificacion;

	@Column(name="nombre_completo")
	private String nombreCompleto;

	//bi-directional many-to-one association to MenuUsuario
	@OneToMany(mappedBy="usuario")
	private List<MenuUsuario> menuUsuarios;

	//bi-directional many-to-one association to PerfilUsuario
	@OneToMany(mappedBy="usuario")
	private List<PerfilUsuario> perfilUsuarios;

	//bi-directional many-to-one association to VariablesConsulta
	@OneToMany(mappedBy="usuario")
	private List<VariablesConsulta> variablesConsultas;	

}