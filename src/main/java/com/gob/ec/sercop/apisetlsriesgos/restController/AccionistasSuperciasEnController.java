package com.gob.ec.sercop.apisetlsriesgos.restController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.gob.ec.sercop.apisetlsriesgos.dto.AccionistasSuperciasEnResponseDto;
import com.gob.ec.sercop.apisetlsriesgos.dto.IdVariableConsultaRequestDto;
import com.gob.ec.sercop.apisetlsriesgos.dto.RespuestaAccionistasSuperciasEnDto;
import com.gob.ec.sercop.apisetlsriesgos.service.AccionistasSuperciasEnService;

@RestController
@RequestMapping("api/accionistasSuperciasEnController")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class AccionistasSuperciasEnController {

	@Autowired
	AccionistasSuperciasEnService accionistasSuperciasEnService;
	
	public static final String MENSAJE = "mensaje";
	
	@PostMapping(value = "mostrarAccionistasSuperciasEn", produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> mostrarAccionistasSuperciasEn(@RequestBody IdVariableConsultaRequestDto idVariableConsultaRequestDto) {
		
		Map<String, Object> responseMap = new HashMap<>();
		RespuestaAccionistasSuperciasEnDto respuestaAccionistasSuperciasEnDto;
		List<AccionistasSuperciasEnResponseDto> accionistasSuperciasEnResponseDto = new ArrayList<>();
		try {
			accionistasSuperciasEnResponseDto= accionistasSuperciasEnService.listaAccionistasSuperciasEnResponseDto(idVariableConsultaRequestDto);
			respuestaAccionistasSuperciasEnDto = new RespuestaAccionistasSuperciasEnDto(true, accionistasSuperciasEnResponseDto);
			
		}catch (Exception e) {
			responseMap.put(MENSAJE, "Error");
			responseMap.put("mensaje", e.toString());
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<RespuestaAccionistasSuperciasEnDto>(respuestaAccionistasSuperciasEnDto, HttpStatus.OK);		
		
	}	
}
