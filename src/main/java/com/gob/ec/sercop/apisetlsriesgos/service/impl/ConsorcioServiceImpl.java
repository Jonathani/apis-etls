package com.gob.ec.sercop.apisetlsriesgos.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gob.ec.sercop.apisetlsriesgos.dao.ConsorcioDao;
import com.gob.ec.sercop.apisetlsriesgos.dto.ConsorcioResponseDto;
import com.gob.ec.sercop.apisetlsriesgos.dto.IdVariableConsultaRequestDto;
import com.gob.ec.sercop.apisetlsriesgos.model.Consorcio;
import com.gob.ec.sercop.apisetlsriesgos.service.ConsorcioService;

@Service
public class ConsorcioServiceImpl implements ConsorcioService{

	@Autowired
	ConsorcioDao consorcioDao;
	
	@Override
	public List<ConsorcioResponseDto> listaConsorcioResponseDto(IdVariableConsultaRequestDto idVariableConsultaRequestDto) {
		
		List<ConsorcioResponseDto> listaConsorcioResponseDto = new ArrayList<>(); 
		List<Consorcio> consorcios = consorcioDao.findByIdVariablesConsulta(Long.parseLong(idVariableConsultaRequestDto.getIdVariableConsulta()));
		for (Consorcio consorcio: consorcios) {
			listaConsorcioResponseDto.add(
					new ConsorcioResponseDto(
							String.valueOf(consorcio.getConsorcioId()),
							consorcio.getPersonaIdConsorcio(), 
							consorcio.getRazonSocialConsorcio(), 
							consorcio.getIdSoliCompra(), 
							consorcio.getFechaRegistro(), 
							consorcio.getPersonaIdParticipante(), 
							consorcio.getCedulaParticipante(), 
							consorcio.getRazonSocialParticipante(), 
							consorcio.getFechaRegistroParticipante(), 
							consorcio.getIdConsorcio())
			);
		}		
		
		return listaConsorcioResponseDto;
	}
	
	

}
