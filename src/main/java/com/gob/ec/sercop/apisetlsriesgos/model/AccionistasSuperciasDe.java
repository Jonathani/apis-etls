package com.gob.ec.sercop.apisetlsriesgos.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * The persistent class for the accionistas_supercias_de database table.
 * 
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="accionistas_supercias_de", schema = "arboles_familiares")
@NamedQuery(name="AccionistasSuperciasDe.findAll", query="SELECT a FROM AccionistasSuperciasDe a")
public class AccionistasSuperciasDe implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "accionistas_supercias_de_id_seq", sequenceName = "public.accionistas_supercias_de_id_seq", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "accionistas_supercias_de_id_seq")
	@Column(name="id_accionistas_supercias_de")
	private long idAccionistasSuperciasDe;

	@Column(name="cedula")
	private String cedula;

	@Column(name="expediente")
	private String expediente;

	@Column(name="nombre")
	private String nombre;

	@Column(name="razon_social")
	private String razonSocial;

	@Column(name="ruc")
	private String ruc;

	//bi-directional many-to-one association to VariablesConsulta
	@ManyToOne
	@JoinColumn(name="id_variables_consulta")
	private VariablesConsulta variablesConsulta;

}