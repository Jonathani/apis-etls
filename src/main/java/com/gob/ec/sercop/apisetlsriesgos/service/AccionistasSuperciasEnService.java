package com.gob.ec.sercop.apisetlsriesgos.service;

import java.util.List;

import com.gob.ec.sercop.apisetlsriesgos.dto.AccionistasSuperciasEnResponseDto;
import com.gob.ec.sercop.apisetlsriesgos.dto.IdVariableConsultaRequestDto;

public interface AccionistasSuperciasEnService {
	
	List<AccionistasSuperciasEnResponseDto> listaAccionistasSuperciasEnResponseDto(IdVariableConsultaRequestDto idVariableConsultaRequestDto);

}
