package com.gob.ec.sercop.apisetlsriesgos.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gob.ec.sercop.apisetlsriesgos.dao.AccionistasSuperciasDeDao;
import com.gob.ec.sercop.apisetlsriesgos.dto.AccionistasSuperciasDeResponseDto;
import com.gob.ec.sercop.apisetlsriesgos.dto.IdVariableConsultaRequestDto;
import com.gob.ec.sercop.apisetlsriesgos.model.AccionistasSuperciasDe;
import com.gob.ec.sercop.apisetlsriesgos.service.AccionistasSuperciasDeService;

@Service
public class AccionistasSuperciasDeServiceImpl implements AccionistasSuperciasDeService{
	
	@Autowired
	AccionistasSuperciasDeDao accionistasSuperciasDeDao;

	@Override
	public List<AccionistasSuperciasDeResponseDto> listaAccionistasSuperciasDeResponseDto(IdVariableConsultaRequestDto idVariableConsultaRequestDto) {
		
		List<AccionistasSuperciasDeResponseDto> listaAccionistasSuperciasDeResponseDto = new ArrayList<>();
		List<AccionistasSuperciasDe> listaAccionistasSuperciasDe =  accionistasSuperciasDeDao.findByIdVariablesConsulta(Long.parseLong(idVariableConsultaRequestDto.getIdVariableConsulta()));
		
		for (AccionistasSuperciasDe accionistasSuperciasDe: listaAccionistasSuperciasDe) {
			listaAccionistasSuperciasDeResponseDto.add(
				new AccionistasSuperciasDeResponseDto(
						String.valueOf(accionistasSuperciasDe.getIdAccionistasSuperciasDe()), 
						accionistasSuperciasDe.getExpediente(), 
						accionistasSuperciasDe.getRuc(), 
						accionistasSuperciasDe.getRazonSocial(), 
						accionistasSuperciasDe.getCedula(), 
						accionistasSuperciasDe.getNombre())
			);
		}		
		return listaAccionistasSuperciasDeResponseDto;
	}
	
	

}
