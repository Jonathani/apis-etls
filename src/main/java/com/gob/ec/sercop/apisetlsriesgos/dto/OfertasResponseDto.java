package com.gob.ec.sercop.apisetlsriesgos.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OfertasResponseDto {
	
	String idOfertas;
	
	String idSoliCompra;
	
	String codigoProcedimiento;
	
	String objectoProceso;
	
	String nombreContratante;
	
	String rucContratante;
	
	String tipoProceso;
	
	String fechaPublicacion;
	
	String presupuesto;
	
	String estado;
	
	String fechaPropuesta;
	
	String valorPropuesta;
	
	String idParticipante;
	
	String razonSocialParticipante;
	
}
