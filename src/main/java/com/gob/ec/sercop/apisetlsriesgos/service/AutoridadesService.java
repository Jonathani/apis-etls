package com.gob.ec.sercop.apisetlsriesgos.service;

import java.util.List;

import com.gob.ec.sercop.apisetlsriesgos.dto.AutoridadesResponseDto;
import com.gob.ec.sercop.apisetlsriesgos.dto.IdVariableConsultaRequestDto;

public interface AutoridadesService {

	List<AutoridadesResponseDto> listaAutoridadesResponseDto(IdVariableConsultaRequestDto idVariableConsultaRequestDto);
}
