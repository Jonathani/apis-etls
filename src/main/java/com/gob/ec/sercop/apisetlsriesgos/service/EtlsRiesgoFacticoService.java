package com.gob.ec.sercop.apisetlsriesgos.service;

import com.gob.ec.sercop.apisetlsriesgos.dto.EtlRiesgoFacticoRequestDto;

public interface EtlsRiesgoFacticoService {
	
	Boolean ejecutarEtlRiesgoFactico(EtlRiesgoFacticoRequestDto etlRiesgoFacticoRequestDto, String idVariablesConsulta);
	
	String registrarEtlRiesgoFactico(EtlRiesgoFacticoRequestDto etlRiesgoFacticoRequestDto);
	
}
