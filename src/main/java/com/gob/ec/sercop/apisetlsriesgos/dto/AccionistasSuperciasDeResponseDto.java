package com.gob.ec.sercop.apisetlsriesgos.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AccionistasSuperciasDeResponseDto {

	String idAccionistrasSuperciasDe;
	
	String expediente;
	
	String ruc;
	
	String razonSocial;
	
	String cedula;
	
	String nombre;
	
}
