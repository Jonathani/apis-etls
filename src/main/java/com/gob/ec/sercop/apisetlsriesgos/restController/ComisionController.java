package com.gob.ec.sercop.apisetlsriesgos.restController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.gob.ec.sercop.apisetlsriesgos.dto.ComisionResponseDto;
import com.gob.ec.sercop.apisetlsriesgos.dto.IdVariableConsultaRequestDto;
import com.gob.ec.sercop.apisetlsriesgos.dto.RespuestaComisionDto;
import com.gob.ec.sercop.apisetlsriesgos.service.ComisionService;

@RestController
@RequestMapping("api/comisionController")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class ComisionController {

	@Autowired
	ComisionService comisionService;
	
	public static final String MENSAJE = "mensaje";
	
	@PostMapping(value = "mostrarComision", produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> mostrarComision(@RequestBody IdVariableConsultaRequestDto idVariableConsultaRequestDto) {
		
		Map<String, Object> responseMap = new HashMap<>();
		RespuestaComisionDto respuestaComisionDto;
		List<ComisionResponseDto> comisionResponseDto = new ArrayList<>();
		
		try {
			comisionResponseDto= comisionService.listaComisionResponseDto(idVariableConsultaRequestDto);
			respuestaComisionDto = new RespuestaComisionDto(true, comisionResponseDto);
			
		}catch (Exception e) {
			responseMap.put(MENSAJE, "Error");
			responseMap.put("mensaje", e.toString());
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<RespuestaComisionDto>(respuestaComisionDto, HttpStatus.OK);		
		
	}
}
