package com.gob.ec.sercop.apisetlsriesgos.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gob.ec.sercop.apisetlsriesgos.model.Consorcio;

@Repository
public interface ConsorcioDao extends CrudRepository<Consorcio, Long>{
	
	@Transactional(readOnly = true)
	@Query(value = "SELECT consorcio "
			+ "FROM Consorcio consorcio "
			+ "JOIN consorcio.variablesConsulta variablesConsulta "			
			+ "WHERE variablesConsulta.idVariablesConsulta = :idVariablesConsulta")
	List<Consorcio> findByIdVariablesConsulta(long idVariablesConsulta);

}
