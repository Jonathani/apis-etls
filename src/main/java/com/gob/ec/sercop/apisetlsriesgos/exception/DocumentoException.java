/**
 * Servicio Nacional de Contratación Pública
 *
 * https://portal.compraspublicas.gob.ec/sercop/
 *
 * Copyright 2023 SERCOP
 *
 * El contenido de este archivo es confidencial y de exclusivo uso del SERCOP.
 */
package com.gob.ec.sercop.apisetlsriesgos.exception;

import java.text.MessageFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

/**
 * <b>Ingrese aqui la descripcion de la clase</b>
 *
 * @author dario.tubon - @date 1 ago. 2022 11:48:37
 *
 */
public class DocumentoException extends Exception {

	private static final long serialVersionUID = 1L;
	@Autowired
	static MessageSource messageSource;

	protected String codigoException;
	protected String mensaje;

	public DocumentoException() {

	}

	public DocumentoException(String codigoException, String... messageArguments) {
		this(getReporteException(codigoException, messageArguments));
		this.codigoException = codigoException;
	}

	public DocumentoException(String mensaje) {
		super(mensaje);
		this.mensaje = mensaje;
	}

	private static String getReporteException(String mensaje, String... messageArguments) {
		if (messageArguments != null) {
			MessageFormat formatter = new MessageFormat("");
			formatter.applyPattern(mensaje);
			mensaje = formatter.format(messageArguments);
		}
		return mensaje;
	}

	public String getCodigoException() {
		return codigoException;
	}

	public void setCodigoException(String codigoException) {
		this.codigoException = codigoException;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	@Override
	public String toString() {
		return mensaje != null ? mensaje : "";
	}
}
