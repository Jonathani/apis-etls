package com.gob.ec.sercop.apisetlsriesgos.restController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.gob.ec.sercop.apisetlsriesgos.dto.RespuestaGenericaReponseDto;
import com.gob.ec.sercop.apisetlsriesgos.dto.VariablesConsultaRequestDto;
import com.gob.ec.sercop.apisetlsriesgos.dto.VariablesConsultaResponseDto;
import com.gob.ec.sercop.apisetlsriesgos.service.VariablesConsultaService;

@RestController
@RequestMapping("api/variablesConsultaController")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class VariablesConsultaController {
	
	@Autowired
	VariablesConsultaService variablesConsultaService;
	
	public static final String MENSAJE = "mensaje";
	
	@PostMapping(value = "mostrarVariablesConsulta", produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> mostrarVariablesConsulta(@RequestBody VariablesConsultaRequestDto variablesConsultaRequestDto) {
		
		Map<String, Object> responseMap = new HashMap<>();
		RespuestaGenericaReponseDto respuestaGenericaReponseDto;
		List<VariablesConsultaResponseDto> variablesConsultaResponseDto = new ArrayList<>(); 
		try {
			variablesConsultaResponseDto = variablesConsultaService.listarVariablesConsulta(variablesConsultaRequestDto);
			respuestaGenericaReponseDto = new RespuestaGenericaReponseDto(true, variablesConsultaResponseDto);
			
		}catch (Exception e) {
			responseMap.put(MENSAJE, "Error");
			responseMap.put("mensaje", e.toString());
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<RespuestaGenericaReponseDto>(respuestaGenericaReponseDto, HttpStatus.OK);
	}
	
}
