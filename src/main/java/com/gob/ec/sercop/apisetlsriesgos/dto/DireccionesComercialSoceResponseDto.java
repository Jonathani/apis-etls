package com.gob.ec.sercop.apisetlsriesgos.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DireccionesComercialSoceResponseDto {

	String idDireccionesComercialSoce;
	
	String cedula;
	
	String razonSocial;
	
	String codPais;
	
	String provincia;
	
	String ciudad;
	
	String parroquia;
	
	String calle;
	
	String interseccion;
	
	String numero;
	
	String edificio;
	
	String departamento;
	
}
