package com.gob.ec.sercop.apisetlsriesgos.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gob.ec.sercop.apisetlsriesgos.dao.ArbolesFamiliaresDao;
import com.gob.ec.sercop.apisetlsriesgos.dto.ArbolesFamiliaresResponseDto;
import com.gob.ec.sercop.apisetlsriesgos.dto.IdVariableConsultaRequestDto;
import com.gob.ec.sercop.apisetlsriesgos.model.ArbolesFamiliares;
import com.gob.ec.sercop.apisetlsriesgos.service.ArbolesFamiliaresService;

@Service
public class ArbolesFamiliaresServiceImpl implements ArbolesFamiliaresService{
	
	@Autowired
	ArbolesFamiliaresDao arbolesFamiliaresDao;

	@Override
	public List<ArbolesFamiliaresResponseDto> listaArbolesFamiliaresResponseDto(IdVariableConsultaRequestDto idVariableConsultaRequestDto) {
		
		List<ArbolesFamiliaresResponseDto> listArbolesFamiliaresResponseDto = new ArrayList<>();
		List<ArbolesFamiliares> listaArbolesFamiliares = arbolesFamiliaresDao.findByIdVariablesConsulta(Long.parseLong(idVariableConsultaRequestDto.getIdVariableConsulta())); 
		
		for (ArbolesFamiliares arbolesFamiliares: listaArbolesFamiliares) {
			listArbolesFamiliaresResponseDto.add(
				new ArbolesFamiliaresResponseDto(
						String.valueOf(arbolesFamiliares.getIdArbolesFamiliares()), 
						arbolesFamiliares.getCedula(),
						arbolesFamiliares.getCedulaConyuge(),
						arbolesFamiliares.getCedulaMadre(), 
						arbolesFamiliares.getCedulaPadre(), 
						arbolesFamiliares.getEstadoCivil(), 
						arbolesFamiliares.getFechaNacimiento(), 
						arbolesFamiliares.getIdentificacionUsuarioConsultado(), 
						arbolesFamiliares.getNivel(), 
						arbolesFamiliares.getNombreConyuge(), 
						arbolesFamiliares.getNombresMadre(), 
						arbolesFamiliares.getNombresPadre(), 
						arbolesFamiliares.getNombresUsuarioConsultado(), 
						arbolesFamiliares.getTipo())
			);
		}
		
		return listArbolesFamiliaresResponseDto;
	}	

}
