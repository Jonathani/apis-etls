package com.gob.ec.sercop.apisetlsriesgos.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gob.ec.sercop.apisetlsriesgos.model.AccionistasSoceDe;

@Repository
public interface AccionistasSoceDeDao extends CrudRepository<AccionistasSoceDe, Long>{

	@Transactional(readOnly = true)
	@Query(value = "SELECT accionistasSoceDe "
			+ "FROM AccionistasSoceDe accionistasSoceDe "
			+ "JOIN accionistasSoceDe.variablesConsulta variablesConsulta "			
			+ "WHERE variablesConsulta.idVariablesConsulta = :idVariablesConsulta")
	List<AccionistasSoceDe> findByIdVariablesConsulta(long idVariablesConsulta);
}
