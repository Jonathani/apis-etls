package com.gob.ec.sercop.apisetlsriesgos.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EtlRiesgoFacticoRequestDto {

	private String fecha;
	
	private String personasExtra;
	
	private String codigos;
	
	private String idUsuario;
		
}
