package com.gob.ec.sercop.apisetlsriesgos.model;

import java.io.Serializable;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * The persistent class for the accionistas_soce_de database table.
 * 
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="accionistas_soce_de", schema = "arboles_familiares")
@NamedQuery(name="AccionistasSoceDe.findAll", query="SELECT a FROM AccionistasSoceDe a")
public class AccionistasSoceDe implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "accionistas_soce_de_id_seq", sequenceName = "public.accionistas_soce_de_id_seq", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "accionistas_soce_de_id_seq")
	@Column(name="id_accionistas_soce_de")
	private long idAccionistasSoceDe;

	@Column(name="accionista_ced_ruc")
	private String accionistaCedRuc;

	@Column(name="accionista_nombres_apellidos")
	private String accionistaNombresApellidos;

	@Column(name="nombre_comercial")
	private String nombreComercial;

	@Column(name="razon_social")
	private String razonSocial;

	@Column(name="ruc")
	private String ruc;

	//bi-directional many-to-one association to VariablesConsulta
	@ManyToOne
	@JoinColumn(name="id_variables_consulta")
	private VariablesConsulta variablesConsulta;
	
}