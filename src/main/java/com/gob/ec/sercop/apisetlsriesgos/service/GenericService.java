/**
 * Servicio Nacional de Contratación Pública
 *
 * https://portal.compraspublicas.gob.ec/sercop/
 *
 * Copyright 2023 SERCOP
 *
 * El contenido de este archivo es confidencial y de exclusivo uso del SERCOP.
 */
package com.gob.ec.sercop.apisetlsriesgos.service;

import java.util.List;

import com.gob.ec.sercop.apisetlsriesgos.exception.DocumentoException;



//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.Pageable;



/**
 * <b>Ingrese aqui la descripcion de la clase</b>
 *
 * @author jonathan.imbaquingo - @date 1 ago. 2022 11:44:09
 *
 */
public interface GenericService<T> {
	public List<T> findAll();

	//public Page<T> findAll(Pageable pageable);

	public T save(T entity) throws DocumentoException;

	public T update(T entity);

	public T findOne(Long id);

	public void delete(Long id);

}
