package com.gob.ec.sercop.apisetlsriesgos.restController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.gob.ec.sercop.apisetlsriesgos.dto.AccionistasSoceDeResponseDto;
import com.gob.ec.sercop.apisetlsriesgos.dto.IdVariableConsultaRequestDto;
import com.gob.ec.sercop.apisetlsriesgos.dto.RespuestaAccionistasSoceDeDto;
import com.gob.ec.sercop.apisetlsriesgos.service.AccionistasSoceDeService;

@RestController
@RequestMapping("api/accionistasSoceDeController")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class AccionistasSoceDeController {
	
	@Autowired
	AccionistasSoceDeService accionistasSoceDeService;
	
	public static final String MENSAJE = "mensaje";
	
	@PostMapping(value = "mostrarAccionistasSoceDe", produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> mostrarAccionistasSoceDe(@RequestBody IdVariableConsultaRequestDto idVariableConsultaRequestDto) {
		
		Map<String, Object> responseMap = new HashMap<>();
		RespuestaAccionistasSoceDeDto respuestaAccionistasSoceDeDto;
		List<AccionistasSoceDeResponseDto> accionistasSoceDeResponseDto = new ArrayList<>();
		try {
			accionistasSoceDeResponseDto= accionistasSoceDeService.listaAccionistasSoceDeResponseDto(idVariableConsultaRequestDto);
			respuestaAccionistasSoceDeDto = new RespuestaAccionistasSoceDeDto(true, accionistasSoceDeResponseDto);
			
		}catch (Exception e) {
			responseMap.put(MENSAJE, "Error");
			responseMap.put("mensaje", e.toString());
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<RespuestaAccionistasSoceDeDto>(respuestaAccionistasSoceDeDto, HttpStatus.OK);	
		
	}
}
