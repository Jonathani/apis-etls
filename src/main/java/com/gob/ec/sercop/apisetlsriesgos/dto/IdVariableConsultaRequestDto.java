package com.gob.ec.sercop.apisetlsriesgos.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class IdVariableConsultaRequestDto {
	
	String idVariableConsulta;

}
