package com.gob.ec.sercop.apisetlsriesgos.model;

import java.io.Serializable;
import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * The persistent class for the comision database table.
 * 
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="comision", schema = "arboles_familiares")
@NamedQuery(name="Comision.findAll", query="SELECT c FROM Comision c")
public class Comision implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "comision_id_seq", sequenceName = "public.comision_id_seq", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "comision_id_seq")
	@Column(name="id_comision")
	private long idComision;

	@Column(name="cargo")
	private String cargo;

	@Column(name="cedula")
	private String cedula;

	@Column(name="funcion")
	private String funcion;

	@Column(name="id_soli_compra")
	private String idSoliCompra;

	@Column(name="nombre")
	private String nombre;

	//bi-directional many-to-one association to VariablesConsulta
	@ManyToOne
	@JoinColumn(name="id_variables_consulta")
	private VariablesConsulta variablesConsulta;	

}