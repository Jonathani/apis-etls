package com.gob.ec.sercop.apisetlsriesgos.service;

import java.util.List;

import com.gob.ec.sercop.apisetlsriesgos.dto.AccionistasSoceDeResponseDto;
import com.gob.ec.sercop.apisetlsriesgos.dto.IdVariableConsultaRequestDto;

public interface AccionistasSoceDeService {
	
	List<AccionistasSoceDeResponseDto> listaAccionistasSoceDeResponseDto(IdVariableConsultaRequestDto idVariableConsultaRequestDto);

}
