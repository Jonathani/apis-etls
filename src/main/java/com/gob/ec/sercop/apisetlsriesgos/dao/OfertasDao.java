package com.gob.ec.sercop.apisetlsriesgos.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gob.ec.sercop.apisetlsriesgos.model.Oferta;

@Repository
public interface OfertasDao extends CrudRepository<Oferta, Long>{
	
	@Transactional(readOnly = true)
	@Query(value = "SELECT oferta "
			+ "FROM Oferta oferta "
			+ "JOIN oferta.variablesConsulta variablesConsulta "			
			+ "WHERE variablesConsulta.idVariablesConsulta = :idVariablesConsulta")
	List<Oferta> findByIdVariablesConsulta(long idVariablesConsulta);
	

}
