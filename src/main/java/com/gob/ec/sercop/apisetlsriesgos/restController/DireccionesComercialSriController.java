package com.gob.ec.sercop.apisetlsriesgos.restController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.gob.ec.sercop.apisetlsriesgos.dto.DireccionesComercialSriResponseDto;
import com.gob.ec.sercop.apisetlsriesgos.dto.IdVariableConsultaRequestDto;
import com.gob.ec.sercop.apisetlsriesgos.dto.RespuestaDireccionesComercialSriDto;
import com.gob.ec.sercop.apisetlsriesgos.service.DireccionesComercialesSriService;

@RestController
@RequestMapping("api/direccionesComercialesSriController")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class DireccionesComercialSriController {

	@Autowired
	DireccionesComercialesSriService direccionesComercialesSriService;
	public static final String MENSAJE = "mensaje";
	
	@PostMapping(value = "mostrarDireccionesComercialesSri", produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> mostrarDireccionesComercialesSri(@RequestBody IdVariableConsultaRequestDto idVariableConsultaRequestDto) {
		
		Map<String, Object> responseMap = new HashMap<>();
		RespuestaDireccionesComercialSriDto respuestaDireccionesComercialSriDto;
		List<DireccionesComercialSriResponseDto> direccionesComercialSriResponseDto = new ArrayList<>();
		
		try {
			direccionesComercialSriResponseDto= direccionesComercialesSriService.listaDireccionesComercialesSriResponseDto(idVariableConsultaRequestDto);
			respuestaDireccionesComercialSriDto = new RespuestaDireccionesComercialSriDto(true, direccionesComercialSriResponseDto);
			
		}catch (Exception e) {
			responseMap.put(MENSAJE, "Error");
			responseMap.put("mensaje", e.toString());
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<RespuestaDireccionesComercialSriDto>(respuestaDireccionesComercialSriDto, HttpStatus.OK);
	}
}
