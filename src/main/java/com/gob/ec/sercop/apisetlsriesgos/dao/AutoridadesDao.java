package com.gob.ec.sercop.apisetlsriesgos.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gob.ec.sercop.apisetlsriesgos.model.Autoridades;

@Repository
public interface AutoridadesDao extends CrudRepository<Autoridades, Long>{

	@Transactional(readOnly = true)
	@Query(value = "SELECT autoridades "
			+ "FROM Autoridades autoridades "
			+ "JOIN autoridades.variablesConsulta variablesConsulta "			
			+ "WHERE variablesConsulta.idVariablesConsulta = :idVariablesConsulta")
	List<Autoridades> findByIdVariablesConsulta(long idVariablesConsulta);
}
