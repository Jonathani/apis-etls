package com.gob.ec.sercop.apisetlsriesgos.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gob.ec.sercop.apisetlsriesgos.dao.AccionistasSuperciasEnDao;
import com.gob.ec.sercop.apisetlsriesgos.dto.AccionistasSuperciasEnResponseDto;
import com.gob.ec.sercop.apisetlsriesgos.dto.IdVariableConsultaRequestDto;
import com.gob.ec.sercop.apisetlsriesgos.model.AccionistasSuperciasEn;
import com.gob.ec.sercop.apisetlsriesgos.service.AccionistasSuperciasEnService;

@Service
public class AccionistasSuperciasEnServiceImpl implements AccionistasSuperciasEnService {

	@Autowired
	AccionistasSuperciasEnDao accionistasSuperciasEnDao;
	
	@Override
	public List<AccionistasSuperciasEnResponseDto> listaAccionistasSuperciasEnResponseDto(
			IdVariableConsultaRequestDto idVariableConsultaRequestDto) {
		
		List<AccionistasSuperciasEnResponseDto> listAccionistasSuperciasEnResponseDto = new ArrayList<>();
		List<AccionistasSuperciasEn> listaAccionistasSuperciasEn = accionistasSuperciasEnDao.findByIdVariablesConsulta(Long.parseLong(idVariableConsultaRequestDto.getIdVariableConsulta()));
		
		for (AccionistasSuperciasEn accionistasSuperciasEn: listaAccionistasSuperciasEn) {
			listAccionistasSuperciasEnResponseDto.add(
				new AccionistasSuperciasEnResponseDto(
					String.valueOf(accionistasSuperciasEn.getIdAccionistasSuperciasEn()), 
					accionistasSuperciasEn.getExpediente(), 
					accionistasSuperciasEn.getRuc(), 
					accionistasSuperciasEn.getRazonSocial(), 
					accionistasSuperciasEn.getCedula(), 
					accionistasSuperciasEn.getNombre())
			);
		}
		
		return listAccionistasSuperciasEnResponseDto;
	}

}
