package com.gob.ec.sercop.apisetlsriesgos.service;

import java.util.List;

import com.gob.ec.sercop.apisetlsriesgos.dto.VariablesConsultaRequestDto;
import com.gob.ec.sercop.apisetlsriesgos.dto.VariablesConsultaResponseDto;

public interface VariablesConsultaService {
	
	List<VariablesConsultaResponseDto> listarVariablesConsulta(VariablesConsultaRequestDto variablesConsultaRequestDto);

}
