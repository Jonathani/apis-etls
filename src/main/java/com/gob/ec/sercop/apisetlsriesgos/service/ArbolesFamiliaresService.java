package com.gob.ec.sercop.apisetlsriesgos.service;

import java.util.List;

import com.gob.ec.sercop.apisetlsriesgos.dto.ArbolesFamiliaresResponseDto;
import com.gob.ec.sercop.apisetlsriesgos.dto.IdVariableConsultaRequestDto;

public interface ArbolesFamiliaresService {
	
	List<ArbolesFamiliaresResponseDto> listaArbolesFamiliaresResponseDto(IdVariableConsultaRequestDto idVariableConsultaRequestDto);

}
